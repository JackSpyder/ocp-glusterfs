s#!/bin/bash
# Delete GlusterFS cluster

# Get settings from the file
source settings

#
gcloud config set project ${PROJECT}

# Delete servers

# Delete Gluster Routes ( visible in gCloud Networking Section )
for (( i=1; i<=${GLUSTER_COUNT}; i++ ))
do
     # Delete the route for VM's static IP
     echo "Deleting the route for ${GLUSTER_NAME}-${i} static IP ${STATIC_IP[$i-1]} ..."
     gcloud compute routes delete ip-${GLUSTER_NAME}-${i} 
     echo " "

     # Delete the compute instances
    gcloud compute instances delete ${GLUSTER_NAME}-${i} --zone ${REGION}-${ZONES[$i-1]}    
    
done

# Delete Master Routes ( visible in gCloud Networking Section )
for (( i=1; i<=${MASTERS_COUNT}; i++ ))
do
     # Delete the route for VM's static IP
     echo "Deleting the route for ${MASTERS_NAME}-${i} static IP ${STATIC_IP[$i-1]} ..."
     gcloud compute routes delete ip-${MASTERS_NAME}-${i} 
     echo " "

     # Delete the compute instances
    gcloud compute instances delete ${MASTERS_NAME}-${i} --zone ${REGION}-${ZONES[$i-1]}    
    
done

# Delete Bastion Routes ( visible in gCloud Networking Section )
for (( i=1; i<=${BASTION_COUNT}; i++ ))
do
     # Delete the route for VM's static IP
     echo "Deleting the route for ${BASTION_NAME}-${i} static IP ${STATIC_IP[$i-1]} ..."
     gcloud compute routes delete ip-${BASTION_NAME}-${i} 
     echo " "

     # Delete the compute instances
    gcloud compute instances delete ${BASTION_NAME}-${i} --zone ${REGION}-${ZONES[$i-1]}    
    
done

# delete Gluster persistent Bricks
for (( i=1; i<=${GLUSTER_COUNT}; i++ ))
do
  gcloud compute disks delete ${BRICK_NAME}1-${i} --zone ${REGION}-${ZONES[$i-1]}
done



echo " "
echo "The end ..."
