#!/bin/bash
# Upgrade GlusterFS server package

# Get settings from the file
source settings

# 
for (( i=1; i<=${GLUSTER_COUNT}; i++ ))
do
  echo "Upgrade GlusterFS server package on ${GLUSTER_NAME}-${i} server ..."
  gcloud compute ssh --zone ${REGION}-${ZONES[$i-1]} ${GLUSTER_NAME}-${i} --command "sudo yum -y install --only-upgrade glusterfs-server"
  echo " "
done
echo " "
