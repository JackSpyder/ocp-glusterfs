#!/bin/bash
# Upgrade Ubuntu 

# Get settings from the file
source settings

# Upgrade Gluster Servers
for (( i=1; i<=${GLUSTER_COUNT}; i++ ))
do
  echo "Run upgrade of packages on ${GLUSTER_NAME}-${i} server ..."
  gcloud compute ssh --zone ${REGION}-${ZONES[$i-1]} ${GLUSTER_NAME}-${i} --command "sudo yum -y update && sudo yum -y upgrade"
  echo " "
done
echo " "

# Upgrade Master Servers
for (( i=1; i<=${MASTERS_COUNT}; i++ ))
do
  echo "Run upgrade of packages on ${MASTERS_NAME}-${i} server ..."
  gcloud compute ssh --zone ${REGION}-${ZONES[$i-1]} ${MASTERS_NAME}-${i} --command "sudo yum -y update && sudo yum -y upgrade"
  echo " "
done
echo " "

# Upgrade Management/Bastion Servers
for (( i=1; i<=${BASTION_COUNT}; i++ ))
do
  echo "Run upgrade of packages on ${BASTION_NAME}-${i} server ..."
  gcloud compute ssh --zone ${REGION}-${ZONES[$i-1]} ${BASTION_NAME}-${i} --command "sudo yum -y update && sudo yum -y upgrade"
  echo " "
done
echo " "
