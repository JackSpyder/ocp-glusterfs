#!/bin/bash
# Provision Nodes and GlusterFS Bricks

# Import Settings Variables
source settings

# Set Gcloud Project Identifier
set_project(){
     gcloud config set project ${PROJECT}     
}

# Create Persistent GlusterFS Bricks
create_gluster_bricks(){
     for (( i=1; i<=${GLUSTER_COUNT}; i++ ))
     do
          (gcloud compute disks create ${BRICK_NAME}1-${i} --size ${BRICK_SIZE} --type ${BRICK_TYPE} --zone ${REGION}-${ZONES[$i-1]}
          echo " ") &
     done
     wait
     echo "Bricks Provisioned"
}

# Node prov
create_node(){
    for (( i=1; i<=${1}; i++ ))
     do
        (
        if [[ $2 == *"gfs"* ]]; then
           gcloud compute instances create ${2}-${CLUSTER_NAME}-server-${i} --image=$IMAGE --image-project=$PRJ_IMG \
               --boot-disk-type pd-standard --boot-disk-size ${3} --zone ${REGION}-${ZONES[$i-1]} \
               --machine-type=${4} \
               --can-ip-forward --tags ${2},${2}-${CLUSTER_NAME}-server,${2}-${CLUSTER_NAME}-server-${i}  \
               --disk name=${BRICK_NAME}1-${i} \
               --scopes compute-rw,logging-write,monitoring,sql-admin,storage-full 
        else
            gcloud compute instances create ${2}-${CLUSTER_NAME}-server-${i} --image=$IMAGE --image-project=$PRJ_IMG \
               --boot-disk-type pd-standard --boot-disk-size ${3} --zone ${REGION}-${ZONES[$i-1]} \
               --machine-type=${4} \
               --can-ip-forward --tags ${2},${2}-${CLUSTER_NAME}-server,${2}-${CLUSTER_NAME}-server-${i}  \
               --scopes compute-rw,logging-write,monitoring,sql-admin,storage-full 
        fi

          echo "Waiting for VM ${2}-${CLUSTER_NAME}-server to be ready..."
          spin='-\|/'
          a=1
          until gcloud compute instances describe --zone ${REGION}-${ZONES[$i-1]} ${2}-${CLUSTER_NAME}-server-${i} | grep "RUNNING" >/dev/null 2>&1; do a=$(( (a+1) %4 )); printf "\r${spin:$a:1}"; sleep .1; done
          sleep 15
          VM_EXT_IP=$(gcloud compute instances list | grep -v grep | grep ${2}-${i}| awk {'print '})
          ssh -q $VM_EXT_IP exit
          echo " "

          # format disk1
          echo "Formating the disk if not formated already ..."
          gcloud compute ssh --zone ${REGION}-${ZONES[$i-1]} ${2}-${CLUSTER_NAME}-server-${i} --command "sudo file -sL /dev/sdb | grep XFS || sudo mkfs.xfs -i size=512 /dev/sdb"
          echo " "

          # Check if node is GlusterFS Node
          if [[ $2 == *"gfs"* ]]; then
               #Mount GlusterFS Brick
               echo "Mount the brick1"
               gcloud compute ssh --zone ${REGION}-${ZONES[$i-1]} ${2}-${CLUSTER_NAME}-server-${i} --command "sudo mkdir -p /data/brick1 && echo '/dev/sdb /data/brick1 xfs defaults 1 2' | sudo tee -a /etc/fstab && sudo mount -a && mount"
               echo " "
               # Install GlusterFS server
               echo "Install GlusterFS server"
               gcloud compute ssh --zone ${REGION}-${ZONES[$i-1]} ${2}-${CLUSTER_NAME}-server-${i} --command "sudo yum -y install glusterfs-server"
               echo " "
          fi

          # install packages and yum update
          echo "Install Base Packages"
          gcloud compute ssh --zone ${REGION}-${ZONES[$i-1]} ${2}-${CLUSTER_NAME}-server-${i} --command "sudo yum install -y \
            bash-completion \
            bind-utils \
            bridge-utils \ 
            centos-release-openshift-origin \
            dos2unix \
            git \
            htop \
            iftop \
            iptables-services \
            kexec-tools \
            mlocate \
            net-tools \ 
            origin-clients \
            psacct \
            sos \
            tree \
            wget"
          echo "Initiate Yum Update"
          gcloud compute ssh --zone ${REGION}-${ZONES[$i-1]} ${2}-${CLUSTER_NAME}-server-${i} --command "sudo yum -y update"

          echo " ") &
     done
     wait
     echo "${2}s Provisioned"
}

# Add Nodes to Trusted Pool
add_to_pool(){
     echo "Configure the trusted pool ..."
     for (( i=1; i<=${1}; i++ ))
     do
          (echo "Configure the trusted pool for ${2}-${CLUSTER_NAME}-server-${i} ..."
          gcloud compute ssh --zone ${REGION}-${ZONES[0]} ${2}-${CLUSTER_NAME}-server-1 --command "sudo gluster peer probe ${2}-${i}") &
     done
     wait
 }

#Add a firewall rule
  create_firewall_rule(){
     gcloud compute firewall-rules create ${1}-firewall-network-1 --allow tcp:80 --target-tags ${1}
}

 create_health_check(){
     gcloud compute http-health-checks create basic-check
 }

# NAME, COUNT, 
 create_loadbalancer(){
    gcloud compute addresses create network-lb-ip-${1} \
    --region ${REGION}

    gcloud compute target-pools create ${1}-pool \
    --region ${REGION}
    --http-health-check basic-check

    
    for (( i=1; i<=${2}; i++ ))
    do
    gcloud compute target-pools add-instances ${1}-pool \
    --instances ${1}-${CLUSTER_NAME}-server-${i} \
    --instances-zone ${REGION}-${ZONES[$i-1]}
     done

    gcloud compute forwarding-rules create ${1}-rule \
    --region ${REGION} \
    --ports 80 \
    --address network-lb-ip-${1} \
    --target-pool ${1}-pool

    gcloud compute forwarding-rules describe ${1}-rule --region ${REGION}
 }

create_gluster_bricks &
wait

create_node "$BASTION_COUNT" "$BASTION_NAME" "$BASTION_BOOT_SIZE" "$BASTION_TYPE" &
create_node "$MASTERS_COUNT" "$MASTERS_NAME" "$MASTERS_BOOT_SIZE" "$MASTERS_TYPE" &
create_node "$GLUSTER_COUNT" "$GLUSTER_NAME" "$GLUSTER_BOOT_SIZE" "$GLUSTER_TYPE" &
wait

add_to_pool "$GLUSTER_COUNT" "$GLUSTER_NAME" &
wait

create_health_check

create_firewall_rule "$MASTERS_NAME"
create_firewall_rule "$GLUSTER_NAME"

create_loadbalancer "$MASTERS_NAME" "$MASTERS_COUNT"
create_loadbalancer "$GLUSTER_NAME" "$GLUSTER_COUNT"

echo " "
echo "Cluster Successfully Created"
echo " "